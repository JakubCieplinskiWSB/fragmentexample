package com.example.fragmentexample;




import android.os.Bundle;
import androidx.fragment.app.Fragment;


import androidx.fragment.app.FragmentActivity;

public class FragmentExampleActivity extends FragmentActivity implements ToolbarFragment.ToolbarListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_example);
    }


    public void onButtonClick(int fontsize, String text) {

        TextFragment textFragment =(TextFragment)
                        getSupportFragmentManager().findFragmentById(R.id.text_fragment);

        textFragment.changeTextProperties(fontsize, text);
    }
}